import React, {createContext, useContext} from 'react';
import {Theme} from '../interfaces';
import {configureTheme, typeTheme, colorTheme} from '../theme';

interface ThemeType {
  theme?: Theme;
  typeTheme?: typeTheme;
  colorTheme?: colorTheme;
}

const ThemeContext = createContext<ThemeType>({
  theme: {} as Theme,
});

export const ThemeProvider: React.FC<ThemeType> = ({
  children,
  typeTheme = 'light',
}) => {
  return (
    <ThemeContext.Provider value={{theme: configureTheme(typeTheme)}}>
      {children}
    </ThemeContext.Provider>
  );
};

export function useTheme() {
  const theme = useContext(ThemeContext);

  return theme;
}
