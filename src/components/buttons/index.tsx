import React from 'react';
import {TouchableOpacity, Text, useColorScheme} from 'react-native';
import {ThemeProvider, useTheme} from '../../context/theme.context';

export const Button: React.FC = () => {
  const {theme} = useTheme();
  const typeTheme = useColorScheme();
  console.log(theme);

  return (
    <ThemeProvider typeTheme={typeTheme || undefined}>
      <TouchableOpacity style={{backgroundColor: theme?.primary}}>
        <Text>Taboa</Text>
      </TouchableOpacity>
    </ThemeProvider>
  );
};
