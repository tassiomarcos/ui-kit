import React from 'react';
import {View, Text} from 'react-native';
import {useTheme} from '../../context/theme.context';

export const Screen: React.FC = ({children}) => {
  const {theme} = useTheme();

  return (
    <View style={{backgroundColor: theme?.backgroundColor, flex: 1}}>
      {children}
      <Text>{JSON.stringify(theme)}</Text>
    </View>
  );
};
