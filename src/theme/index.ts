import palette from './palette';
import {Theme} from 'src/interfaces';

export type typeTheme = 'dark' | 'light';

export type colorTheme = keyof typeof palette.light;

export function configureTheme(
  typeTheme: typeTheme,
  colorTheme?: colorTheme,
  defaultTheme?: Theme,
): Theme {
  if (defaultTheme) {
    return defaultTheme;
  }

  return {
    backgroundColor: palette[typeTheme].gray6,
    hightLightColor: palette[typeTheme].gray1,
    primary: palette[typeTheme][colorTheme || 'blue'],
  };
}
