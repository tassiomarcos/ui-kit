export type Theme = {
  backgroundColor: string;
  hightLightColor: string;
  primary: string;
};
