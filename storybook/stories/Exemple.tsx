import React from 'react';
import {View, Text} from 'react-native';
import {storiesOf} from '@storybook/react-native';

type Props = {text: string};

const Example: React.FC<Props> = (props) => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>{props.text}</Text>
    </View>
  );
};

export default Example;
