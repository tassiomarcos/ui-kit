import React from 'react';
import {Screen} from '../../../src/components/screen';
import {Button} from '../../../src/components/buttons';
import {ThemeProvider} from '../../../src/context/theme.context';
import {useColorScheme} from 'react-native';

type Props = {text: string};

const Example: React.FC<Props> = (props) => {
  return (
    <ThemeProvider typeTheme={useColorScheme()}>
      <Screen>
        <Button />
      </Screen>
    </ThemeProvider>
  );
};

export default Example;
